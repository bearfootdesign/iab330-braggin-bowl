﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITests
{
    [TestFixture(Platform.Android)]
    //[TestFixture(Platform.iOS)]
    public class Tests
    {
        IApp app;
        Platform platform;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        private static Random random = new Random();
        public static string RandomString(int length) {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public static string RandomInt(int length) {
            const string chars = "123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        [Test]
        public void TestPlayerCreation()
        {

            string gamertag = "TestGamer" + RandomString(10);
            string name = "Test Gamer " + RandomString(10);
            string tagline = "Test Tagline " + RandomString(10);
            string password = RandomString(10);

            //Login Page
            app.Tap("Register");

            app.WaitForElement("TaglineEntry");
            //Player Creation Page
            app.Tap("GamertagEntry");
            app.EnterText(gamertag);
            app.Back();
            app.Tap("NameEntry");
            app.EnterText(name);
            app.Back();
            app.Tap("TaglineEntry");
            app.EnterText(tagline);
            app.Back();
            app.Tap("Password1");
            app.EnterText(password);
            app.Back();
            app.Tap("Password2");
            app.EnterText(password);
            app.Back();
            app.Tap("Create Account");

            app.WaitForElement("playerGamertag");

            //Player Profile
            AppResult[] gamertagResults = app.WaitForElement(c => c.Marked(gamertag));
            AppResult[] nameResults = app.WaitForElement(c => c.Marked(name));
            AppResult[] taglineResults = app.WaitForElement(c => c.Marked(tagline));
            app.Screenshot("ProfilePage for test user");

            Assert.IsTrue(gamertagResults.Any() && nameResults.Any() && taglineResults.Any());
        }

        [Test]
        public void testTournamentCreation() {
            app.Tap("GamertagEntry");
            app.EnterText("Jasper1");
            app.Back();
            app.Tap("PasswordEntry");
            app.EnterText("asd");
            app.Back();
            app.Tap("Login");

            app.WaitForElement("playerGamertag");

            app.DragCoordinates(0, 100, 1000, 100);

            string tournamentType = "Double Elimination";
            string legalTournament = "No";
            string game = RandomString(10);
            string modifiers = RandomString(10);
            string date = DateTime.Now.ToString("MM/dd/yyyy");
            string teamSize = RandomInt(2);

            app.Tap("Create Tournament");
            app.Tap("TournamentTypePicker");
            app.Tap("Double Elimination");
            app.Tap("LegalTournamentPicker");
            app.Tap("No");
            app.Tap("GameEntry");
            app.EnterText(game);
            app.Back();
            app.Tap("Modifiers");
            app.EnterText(modifiers);
            app.Back();
            app.ScrollDown(strategy: ScrollStrategy.Gesture);
            app.Tap("DateEntry");
            app.Tap("OK");
            app.Tap("TeamSize");
            app.EnterText(teamSize);
            app.Back();
            app.Tap("Create Tournament");
            app.Tap("Ok");
            app.DragCoordinates(0, 100, 1000, 100);
            app.Tap("Tournaments");
            //app.ScrollDown(strategy: ScrollStrategy.Gesture);
            app.ScrollDownTo(game, strategy: ScrollStrategy.Gesture, timeout: new TimeSpan(0, 1, 0));
            app.Repl();

            AppResult[] tournamentTypeResults = app.WaitForElement(c => c.Marked("DOUBLE_ELIM"));
            AppResult[] legalTournamentResults = app.WaitForElement(c => c.Marked("NO"));
            AppResult[] gameResults = app.WaitForElement(c => c.Marked(game));
            AppResult[] modifiersResults = app.WaitForElement(c => c.Marked(modifiers));
            AppResult[] dateResults = app.WaitForElement(c => c.Marked(date+" 00:00:00"));
            AppResult[] teamSizeResults = app.WaitForElement(c => c.Marked(teamSize));
            app.Screenshot("Tournaments");

            Assert.IsTrue(tournamentTypeResults.Any() && legalTournamentResults.Any() && gameResults.Any() && modifiersResults.Any() && dateResults.Any() && teamSizeResults.Any());

        }
    }
}
