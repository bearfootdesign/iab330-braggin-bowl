﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITests
{
    public class AppInitializer
    {
        public static IApp StartApp(Platform platform)
        {
            if (platform == Platform.Android)
            {
                return ConfigureApp

                    .Android

                    .ApkFile("../../../bragginBowl/bragginBowl.Android/bin/Debug/com.companyname.bragginBowl.apk")

                    .StartApp();
            }

            return ConfigureApp.iOS.StartApp();
        }
    }
}